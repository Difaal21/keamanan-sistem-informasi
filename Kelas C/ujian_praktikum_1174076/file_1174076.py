import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE','ujian_praktikum_1174076.settings')

import django
django.setup()

import random

from ujian_aplikasi_1174076.models import User
from faker import Faker

def po_1174076(N=90):
    for entry in range(N):
        fake = Faker()
        first_name = fake.first_name()
        last_name = fake.last_name_male()
        email =  fake.email()
        
        data = User.objects.get_or_create(first_name=first_name, last_name=last_name, email=email)[0]

if __name__ == '__main__':
    print("Insert to database... Please Wait !")
    po_1174076(30)
    print("Process Complete !")


